import React, {useEffect, useState} from 'react';
import { Row, Col, Modal, Form, Input } from 'antd';
import toast from "react-hot-toast";
import User from '../../component/user/user'
import Loader from "../../component/loader/loader";
import axios from "axios";

const Home = (props) => {
    const [users, setUsers] = useState([])
    const getUser = () => {
        axios.get('https://jsonplaceholder.typicode.com/users').then(res =>{
           setUsers([...res.data.map(v => ({...v, like: false}))])
        }).catch(error => {
            toast.error("Please try after some time")
        })
    }

    useEffect(() => {
        getUser()
    }, [])

    /**
     * @param id = id of user
     * @param value = is a user like or dislike
     */
    const handleLike = (id, value) => {
        let copyUser = users
        for (let i=0; i<copyUser?.length; i++) {
            if (copyUser[i]?.id == id) {
                copyUser[i].like = value
            }
        }
        setUsers([...copyUser])
    }


    /**
     * @param id = id of user
     */
    const handleDelete = (id) => {
        let copyUser = users
        for (let i=0; i<copyUser?.length; i++) {
            if (copyUser[i]?.id == id) {
                copyUser.splice(i, 1)
            }
        }
        setUsers([...copyUser])
    }

    /**
     * @param id = id of user
     * @param data = is a updated user data
     */
    const handleUpdate = (id, data) => {
        let copyUser = users
        for (let i = 0; i < copyUser?.length; i++) {
            if (copyUser[i]?.id == id) {
                copyUser[i] = {...copyUser[i], ...data}
            }
        }
        setUsers([...copyUser])
    }

    return  <Row>
        {
            users.length == 0 ? <Loader/> : users?.map((user) => (
                <Col data-testid="user" xs={24} sm={24} md={8} lg={8} xl={6} key={user.username}>
                    <User user={user} liked={handleLike} delete={handleDelete} updateData={handleUpdate}/>
                </Col>
            ))
        }
    </Row>
}

export default Home
