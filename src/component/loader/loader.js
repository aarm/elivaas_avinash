import React from 'react';

const Loader = () => {

    return <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', margin: 'auto'}}>
        <div className="spinner">
            <div className="bounce1" />
            <div className="bounce2" />
            <div className="bounce3" />
        </div>
    </div>
}

export default Loader;
