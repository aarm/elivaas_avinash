import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import User from './user'

describe('Post Actions', () => {
    let post;

    beforeEach(() => {
        // Set up a mock post object
        post =  [{
            "id": 1,
            "like": false,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "address": {
                "street": "Kulas Light",
                "suite": "Apt. 556",
                "city": "Gwenborough",
                "zipcode": "92998-3874",
                "geo": {
                    "lat": "-37.3159",
                    "lng": "81.1496"
                }
            },
            "phone": "1-770-736-8031 x56442",
            "website": "hildegard.org",
            "company": {
                "name": "Romaguera-Crona",
                "catchPhrase": "Multi-layered client-server neural-net",
                "bs": "harness real-time e-markets"
            }
        }]
    });

    afterEach(() => {
        // Clean up the post object after each test
        post = null;
    });

    it('User can like a post', () => {
        render(<User user={post[0]} liked={() => {}} delete={() => {}} updateData={() => {}}/>);
        const like = screen.queryByTestId("like");
        fireEvent.click(like);
        post[0].like = true
        expect(post[0].like).toBe(true);
    });

    it('User can delete a post', () => {
        render(<User user={post} liked={() => {}} delete={() => {}} updateData={() => {}} />);
        const deleteButton = screen.queryByTestId("delete");
        fireEvent.click(deleteButton);
        post.length = 0
        expect(post.length).toBe(0);
    });
});
