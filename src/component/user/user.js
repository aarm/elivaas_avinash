import React, {useEffect, useState} from 'react';
import {Button, Card, Form, Input, Modal} from 'antd';
import {
    DeleteFilled,
    EditOutlined,
    GlobalOutlined, HeartFilled,
    HeartOutlined,
    MailOutlined,
    PhoneOutlined
} from "@ant-design/icons";

const User = (props) => {
    const [form] = Form.useForm()
    const {user} = props
    const [showEditModal, setShowEditModal] = useState(false)

    /*
      This function run user click on submit
      @params values is equal to the updated value of form
     */
    const onFinish = (values) => {
        props.updateData(user.id, values)
        setShowEditModal(false)
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    ///// This use set the user data in form to show prefilled
    useEffect(() => {
        //companyInfo -> dynamic obj fetched from API
        form.setFieldsValue(user);
    }, [showEditModal]);

    return <>
        <Modal   footer={null} title="Basic Modal" visible={showEditModal} onCancel={() => setShowEditModal(false)}>
            <Form
                name="basic"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                style={{ maxWidth: 600 }}
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                form={form}
            >
                <Form.Item
                    label="Name"
                    name="name"
                    rules={[{ required: true, message: 'Please input your name!' }]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your Email!' }]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Phone number"
                    name="phone"
                    rules={[{ required: true, message: 'Please input your phone number!' }]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>

        </Modal>

        <Card
            style={{ margin: 15 }}
            cover={
                <div className="user_profile">
                    <img
                        src={`https://avatars.dicebear.com/v2/avataaars/${user?.username}.svg?options[mood][]=happy`}
                        alt="Avatar"
                        style={{ width: 200, height: 200 }}
                    />
                </div>
            }
            actions={[
                <button
                    className='cardButton'
                >
                    {
                        user?.like ? <HeartFilled data-testid="dislike"  onClick={() => props?.liked(user.id, false)} style={{ color: '#FF0000', fontSize: 20 }}/> :   <HeartOutlined data-testid="like"  onClick={() => props?.liked(user.id, true)} style={{ color: '#FF0000', fontSize: 20 }} />

                    }
                </button>,
                <button
                    className='cardButton'
                    onClick={() => setShowEditModal(true)}
                >
                    <EditOutlined  style={{ fontSize: 18 }}/>
                </button>,
                <button
                  className='cardButton'
                    onClick={() => {
                        props?.delete(user.id);
                    }}
                >
                    <DeleteFilled data-testid="delete" style={{ fontSize: 18 }} />
                </button>,
            ]}
        >
            <h3>{user.name}</h3>
            <div className='center'>
                <MailOutlined  style={{ fontSize: '18px' }}/>
                <p style={{ marginLeft: 10 }}>{user.email}</p>
            </div>
            <div className='center'>
                <PhoneOutlined style={{ fontSize: '18px' }}/>
                <p style={{ marginLeft: 10 }}>{user.phone}</p>
            </div>
            <div className='center'>
                <GlobalOutlined style={{ fontSize: '18px' }}/>
                <p style={{ marginLeft: 10 }}>http://{user.website}</p>
            </div>
        </Card>
    </>
}

export default User;
